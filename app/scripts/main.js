var Mediator = (function () {

    var channels = {};

    //
    // Subscribe to a channel
    //
    function subscribe(channel, fn, that) {
        if (!channels[channel]) {
            channels[channel] = [];
        }
        channels[channel].push({
            context: that,
            callback: fn
        });
    }

    //
    // Publish a channel (like triggering an event)
    //
    function publish(channel) {

        if (!channels[channel]) {
            return false;
        }
        var args = Array.prototype.slice.call(arguments, 1);
        for (var i = 0, l = channels[channel].length; i < l; i += 1) {
            var subscription = channels[channel][i];
            if (typeof subscription.context === undefined) {
                subscription.callback(args);
            } else {
                subscription.callback.call(subscription.context, args);
            }
        }
    }

    return {
        subscribe: subscribe,
        publish: publish
    }

})();
console.log("...");


$('html').on('mousedown', function () {
    Mediator.publish('start');
});

$('.slot').on("animationend", function(){
    Mediator.publish("animationend");
});
$('.slot').on("webkitAnimationEnd", function(){
    Mediator.publish("animationend");
});
$('.slot').on("animationstart", function(){
    Mediator.publish("animationstart");
});
$('.slot').on("webkitAnimationStart", function(){
    Mediator.publish("animationstart");
});


var BackendConnector = (function () {
    function send() {
        $.ajax({
            url: "http://dragonquest.at/numbers.php?jsonp",
            dataType: 'jsonp',
            success: function (data) {
                Mediator.publish('datareceived',data);
            },
            error: function (err) {
            	 alert("Fehler " + err);
            }
        });
    }

    Mediator.subscribe('inputGiven', send);

})();

$('html').on('mousedown', function () {

    if(isRunning === false)
    {
        Mediator.publish('inputGiven');
    }
});

var gameData;
var coins = 200;
var animations = 0;
var isRunning = false;
$("#money").text(coins);

Mediator.subscribe('datareceived', function (data) {
    gameData = data;
    console.log("Meine Daten sind hier " + gameData);
    window.requestAnimFrame(function(){
        Mediator.publish("clearFrame");
    });
});

Mediator.subscribe('clearFrame', function(data){
    clearSlots();
    window.requestAnimFrame(function(){
        Mediator.publish("setFrame");
    });
});

Mediator.subscribe('setFrame', function(data){
   setNewPosition(gameData[0].slots);
});


Mediator.subscribe('animationstart', function(data){
    isRunning = true;
    animations++;
    console.log("X"+animations);
    $("#cherrySound")[0].play();
});

Mediator.subscribe('animationend', function(data){
    animations--;
    console.log("X"+animations)
    if(animations == 0)
    {        
        if(gameData[0].result <= 0)
        {
            $("#loseSound")[0].play();
        }else{
            $("#winSound")[0].play();
        }
        coins += gameData[0].result;
        console.log("Coins: " + coins);
        $("#money").text(coins);
        isRunning = false;
    }
});

$("#cherrySound").on("ended", function(){
    if(isRunning)
    {
        Mediator.publish("playTicks");
    }
});

Mediator.subscribe("playTicks", function(){
    $("#cherrySound")[0].play();
})

function clearSlots(){
$(".slot").removeClass("cherry");
$(".slot").removeClass("star");
$(".slot").removeClass("character");
$(".slot").removeClass("vegetable");
}

function setNewPosition(slotsArray){
	clearSlots();
	$("#leftSlot").addClass(getClassForNumber(slotsArray[0]));
	$("#middleSlot").addClass(getClassForNumber(slotsArray[1]));
	$("#rightSlot").addClass(getClassForNumber(slotsArray[2]));
}

function getClassForNumber (number) {
	console.log(number);
	if (number == 0) {return "cherry"};
	if (number == 1) {return "character"};
	if (number == 2) {return "vegetable"};
	if (number == 3) {return "star"};
}

window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 60);
          };
})();
